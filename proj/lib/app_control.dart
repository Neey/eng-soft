import 'package:flutter/cupertino.dart';

class AppControl extends ChangeNotifier {
  static AppControl instance = AppControl();

  bool isdarktheme = false;
  changeTheme() {
    isdarktheme = !isdarktheme;
    notifyListeners();
  }
}
