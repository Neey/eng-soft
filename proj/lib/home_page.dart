import 'package:flutter/material.dart';
import 'package:proj/Insc.dart';
import 'package:proj/app_control.dart';
import 'package:proj/login_page.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
          child: Column(children: [
            UserAccountsDrawerHeader(
                currentAccountPicture: Image.asset('assets/images/Perfil.jpg'),
                accountName: Text('Ney Oliveira'),
                accountEmail: Text('ney@gmail.com')),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Inicio'),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.calendar_today),
              title: Text('Incrições'),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => InscPage(),
                ));
              },
            ),
            ListTile(
              leading: Icon(Icons.approval),
              title: Text('Orientador'),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.receipt_long_outlined),
              title: Text('Certificados'),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.book),
              title: Text('Anais'),
              onTap: () {},
            ),
            ListTile(
              leading: Icon(Icons.book),
              title: Text('Sair'),
              onTap: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => LoginPage())),
            ),
          ]),
        ),
        appBar: AppBar(
          title: Text('UFRB Eventos'),
        ));
  }
}
