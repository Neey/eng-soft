import 'package:flutter/material.dart';
import 'package:proj/cadastro_Tela.dart';
import 'package:proj/home_page.dart';
import 'package:proj/recuperar.dart';
import 'Insc.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email = '';
  String senha = '';

  Widget _body() {
    return SingleChildScrollView(
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 200,
                  height: 100,
                  child: Image.asset('assets/images/login.png'),
                ),
                Container(height: 5),
                TextField(
                  onChanged: (text) {
                    email = text;
                  },
                  keyboardType: TextInputType.emailAddress,
                  style: new TextStyle(color: Colors.black, fontSize: 20),
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.email,
                      color: Colors.blueAccent,
                    ),
                    labelStyle: TextStyle(color: Colors.lightBlueAccent),
                    labelText: 'Email',
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 10),
                TextField(
                  onChanged: (text) {
                    senha = text;
                  },
                  obscureText: true,
                  style: new TextStyle(color: Colors.black, fontSize: 20),
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.lock,
                      color: Colors.blueAccent,
                    ),
                    labelStyle: TextStyle(color: Colors.lightBlueAccent),
                    labelText: 'Senha',
                    border: OutlineInputBorder(),
                  ),
                ),
                Container(
                  height: 30,
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    child: Text(
                      "Recuperar Senha",
                      textAlign: TextAlign.right,
                    ),
                    onPressed: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => RecuperaSenha()),
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: FlatButton(
                    onPressed: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => CadastroTela()),
                    ),
                    child: Center(
                        child:
                            Container(height: 20, child: Text('Criar conta'))),
                    color: Colors.blue,
                  ),
                ),
                RaisedButton(
                  onPressed: () {
                    if (email == 'test@gmail.com' && senha == '123') {
                      Navigator.of(context).pushReplacementNamed('/home');
                    } else {
                      print(" Email ou senha incorreto");
                    }
                  },
                  child: Center(
                    child: Container(
                      height: 20,
                      child: Text(
                        ' Entrar',
                      ),
                    ),
                  ),
                  color: Colors.blue,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: [
          SizedBox(
              height: MediaQuery.of(context).size.height,
              child: Image.asset(
                'assets/images/blue.png',
                fit: BoxFit.fill,
              )),
          _body(),
        ],
      )),
    );
  }

  Widget buildEmailRow() {
    return Padding(
      padding: EdgeInsets.all(8),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        onChanged: (value) {
          setState(() {
            email = value;
          });
        },
      ),
    );
  }

  Widget buildRecuperarSenha() {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
        onPressed: () => print('Pressionar botão recuperar senha'),
        padding: EdgeInsets.only(right: 0.0),
        child: Text('Recuperar senha?'),
        color: Colors.blue,
      ),
    );
  }

  Widget buildSenhaRow() {
    return Padding(
      padding: EdgeInsets.all(8),
      child: TextFormField(
        keyboardType: TextInputType.text,
        onChanged: (value) {
          setState(() {
            senha = value;
          });
        },
      ),
    );
  }
}
