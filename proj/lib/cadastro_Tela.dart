import 'package:flutter/material.dart';
import 'package:proj/login_page.dart';

class CadastroTela extends StatefulWidget {
  @override
  _CadastroTelaState createState() => _CadastroTelaState();
}

class _CadastroTelaState extends State<CadastroTela> {
  String nome = '';
  String email = '';
  String senha = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 200,
                height: 100,
                child: Image.asset('assets/images/login.png'),
              ),
              TextField(
                onChanged: (text) {
                  nome = text;
                },
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.people,
                    color: Colors.blueAccent,
                  ),
                  labelStyle: TextStyle(color: Colors.lightBlueAccent),
                  labelText: 'Nome',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                onChanged: (text) {
                  email = text;
                },
                keyboardType: TextInputType.emailAddress,
                style: new TextStyle(color: Colors.black, fontSize: 20),
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.email,
                    color: Colors.blueAccent,
                  ),
                  labelStyle: TextStyle(color: Colors.lightBlueAccent),
                  labelText: 'Email',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                onChanged: (text) {
                  senha = text;
                },
                obscureText: true,
                style: new TextStyle(color: Colors.black, fontSize: 20),
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.blueAccent,
                  ),
                  labelStyle: TextStyle(color: Colors.lightBlueAccent),
                  labelText: 'Senha',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              RaisedButton(
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(),
                  ),
                ),
                child: Text(' Cadastrar'),
                color: Colors.blue,
              )
            ],
          ),
        ),
      ),
    ));
  }
}

Widget buildSenhaRow() {
  return Padding(
    padding: EdgeInsets.all(8),
    child: TextFormField(
      keyboardType: TextInputType.text,
    ),
  );
}

Widget buildEmailRow() {
  return Padding(
    padding: EdgeInsets.all(8),
    child: TextFormField(
      keyboardType: TextInputType.emailAddress,
    ),
  );
}

Widget buildNameRow() {
  return Padding(
    padding: EdgeInsets.all(8),
    child: TextFormField(
      keyboardType: TextInputType.name,
    ),
  );
}
