import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InscPage extends StatefulWidget {
  @override
  _InscPageState createState() => _InscPageState();
}

class _InscPageState extends State<InscPage> {
  String evento = '';
  String institui = '';
  String cel = '';
  String categoria = '';
  String partici = '';

  Widget build(BuildContext) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Realizar inscrição'),
        ),
        body: SingleChildScrollView(
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextField(
                    onChanged: (text) {
                      evento = text;
                    },
                    keyboardType: TextInputType.emailAddress,
                    style: new TextStyle(color: Colors.black, fontSize: 20),
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.festival,
                        color: Colors.blueAccent,
                      ),
                      labelStyle: TextStyle(color: Colors.lightBlueAccent),
                      labelText: 'Evento',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    onChanged: (text) {
                      institui = text;
                    },
                    style: new TextStyle(color: Colors.black, fontSize: 20),
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.house,
                        color: Colors.blueAccent,
                      ),
                      labelStyle: TextStyle(color: Colors.lightBlueAccent),
                      labelText: 'Instituição',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextField(
                    onChanged: (text) {
                      cel = text;
                    },
                    style: new TextStyle(color: Colors.black, fontSize: 20),
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.phone,
                        color: Colors.blueAccent,
                      ),
                      labelStyle: TextStyle(color: Colors.lightBlueAccent),
                      labelText: 'Celular',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextField(
                    onChanged: (text) {
                      categoria = text;
                    },
                    style: new TextStyle(color: Colors.black, fontSize: 20),
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.people,
                        color: Colors.blueAccent,
                      ),
                      labelStyle: TextStyle(color: Colors.lightBlueAccent),
                      labelText: 'Categoria',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextField(
                    onChanged: (text) {
                      partici = text;
                    },
                    style: new TextStyle(color: Colors.black, fontSize: 20),
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.person,
                        color: Colors.blueAccent,
                      ),
                      labelStyle: TextStyle(color: Colors.lightBlueAccent),
                      labelText: 'Participação',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  RaisedButton(
                    onPressed: () {},
                    child: Text(' Cadastrar'),
                    color: Colors.blue,
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
